<?php

/**
 * Allcash Module for Fcontrol
 *
 * @title      Magento -> Custom Module for Fcontrol
 * @category   Fraud Control Gateway
 * @package    Allcash_Fcontrol
 * @author     Allcash Team
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @copyright  Copyright (c) 2013 Allcash
 */
class Allcash_Fcontrol_Model_Mysql4_Orders_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

    public function _construct() {
        $this->_init('fcontrol/orders', 'id');
    }

    /**
     * Filter by status
     *
     * @param string $status
     * @return Allcash_Fcontrol_Model_Mysql4_Orders_Collection
     */
    public function addStatusFilter($status) {
        $this->addFieldToFilter('main_table.status', $status);
        return $this;
    }

    /**
     * Filter by abandoned
     *
     * @param int $abandoned
     * @return Allcash_Fcontrol_Model_Mysql4_Orders_Collection
     */
    public function addAbandonedFilter($abandoned) {
        $this->addFieldToFilter('main_table.abandoned', $abandoned);
        return $this;
    }
    
    /**
     * Filter Time  
     *
     * @param integer $time
     * @return Allcash_Fcontrol_Model_Mysql4_Orders_Collection
     */
    public function addTimeFilter() {
        $time = Mage::getStoreConfig('allcash/allcash_mc/tempo_espera');
        if( !$time || $time < 0 ){
            $time = 1;
        }
        
        $this->addFieldToFilter('main_table.updated_at',  array('to'=>Mage::getModel('core/date')->date("Y-m-d H:i:s", strtotime("-{$time} hours") ) ) );
        return $this;
    }    

}

