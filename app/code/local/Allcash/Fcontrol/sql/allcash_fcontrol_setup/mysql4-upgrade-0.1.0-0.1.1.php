<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('fcontrol/orders'), 'store_id', 'int(11) NOT NULL');

$installer->endSetup();

