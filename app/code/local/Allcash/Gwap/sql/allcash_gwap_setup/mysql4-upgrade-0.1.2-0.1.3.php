<?php

$installer = $this;
$installer->startSetup();


$installer->getConnection()
    ->changeColumn($installer->getTable('gwap/oneclick'), 'registration_info', 'registration_id', 'VARCHAR(100)');

$installer->getConnection()
    ->addColumn($installer->getTable('gwap/oneclick'), 'type', 'VARCHAR(20) AFTER cc_last4');

$installer->endSetup();