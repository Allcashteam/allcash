<?php

$installer = $this;
$installer->startSetup();


$installer->getConnection()
    ->addColumn($installer->getTable('gwap/oneclick'), 'cc_last4', 'VARCHAR(10) AFTER registration_info');
$installer->endSetup();