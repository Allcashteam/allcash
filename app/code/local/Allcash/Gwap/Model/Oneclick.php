<?php

/**
 * Allcash - Gwap Payment Module
 *
 * @title      Magento -> Custom Payment
 * @category   Payment Gateway
 * @package    Allcash_Gwap
 * @author     Allcash Development Team
 * @copyright  Copyright (c) 2013 Allcash
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Allcash_Gwap_Model_Oneclick extends Mage_Core_Model_Abstract {

    public function _construct() {
        $this->_init('gwap/oneclick');
    }   

}