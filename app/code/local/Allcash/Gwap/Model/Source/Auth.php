<?php

/**
 * Allcash - Gwap Payment Module
 *
 * @title      Magento -> Custom Payment Module for Gwap
 * @category   Payment Gateway
 * @package    Allcash_Gwap
 * @author     Allcash Development Team
 * @copyright  Copyright (c) 2013 Allcash
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Allcash_Gwap_Model_Source_Auth {

    public function toOptionArray() {
        return array(
            array('value' => '0', 'label' => Mage::helper('gwap')->__('Lote')),
            array('value' => '1', 'label' => Mage::helper('gwap')->__('Instantânea'))
            
        );
    }

}