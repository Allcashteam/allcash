<?php

/**
 * Allcash - AllPago Payment Module
 *
 * @title      Magento -> Custom Payment Module for AllPago
 * @category   Payment Gateway
 * @package    Allcash_AllPago
 * @author     Allcash Development Team
 * @copyright  Copyright (c) 2013 Allcash
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Allcash_Gwap_Block_Info_2cc extends Mage_Payment_Block_Info {

    /**
     * Prepare credit card related payment info
     *
     * @param Varien_Object|array $transport
     * @return Varien_Object
     */
    protected function _prepareSpecificInformation($transport = null)
    {
        if (null !== $this->_paymentSpecificInformation) {
            return $this->_paymentSpecificInformation;
        }
        $transport = parent::_prepareSpecificInformation($transport);
        $data = array();
        
        if ($this->getInfo()->getCcType()) {
            $data[Mage::helper('payment')->__('Credit Card Type')] = $this->getInfo()->getCcType().' | '.$this->getInfo()->getAdditionalInformation('gwapCcType2');
        }
        if ($this->getInfo()->getCcLast4()) {
            $data[Mage::helper('payment')->__('Credit Card Number')] = sprintf('xxxx-%s', $this->getInfo()->getCcLast4()).' | '.sprintf('xxxx-%s', $this->getInfo()->getAdditionalInformation('gwapCcLast4_2'));
        }
        if ($this->getInfo()->getCcParcelas()) {
            $data[Mage::helper('payment')->__('Parcelamento')] = $this->getInfo()->getCcParcelas().'x'.' | '.$this->getInfo()->getAdditionalInformation('gwapCcParcelas2').'x';
        }
        
        if( $this->getInfo()->getOrder() && $this->getInfo()->getOrder()->hasData() ){
            
            $orderID = $this->getInfo()->getOrder()->getId();
            if($orderID){
                $gwap = Mage::getModel('gwap/order')->load($orderID, 'order_id');
                if($gwap->hasData()){
                    if($gwap->getCaptureResult()){
                    
                        $TID = array();
                        $TID = unserialize($gwap->getCaptureResult());

                        if(isset($TID['ConnectorTxID1'])){
                             $data['ConnectorTxID1'] = $TID['ConnectorTxID1'];
                        }
                        if(isset($TID['ConnectorTxID2'])){
                            $data['ConnectorTxID2'] = $TID['ConnectorTxID2'];
                        }        
                        if(isset($TID['ConnectorTxID3'])){
                            $data['ConnectorTxID3'] = $TID['ConnectorTxID3'];
                        }
                        if(isset($TID['LR'])){
                            $data['LR'] = $TID['LR'];
                        }
                        if(isset($TID['NSU'])){
                            $data['NSU'] = $TID['NSU'];
                        }                     
                    }
                    if($gwap->getCaptureResult2()){
                        
                        $TID2 = array();
                        $TID2 = unserialize($gwap->getCaptureResult2());

                        if(isset($TID2['ConnectorTxID1'])){
                             $data['ConnectorTxID1_2'] = $TID2['ConnectorTxID1'];
                        }
                        if(isset($TID2['ConnectorTxID2'])){
                            $data['ConnectorTxID2_2'] = $TID2['ConnectorTxID2'];
                        }        
                        if(isset($TID2['ConnectorTxID3'])){
                            $data['ConnectorTxID3_2'] = $TID2['ConnectorTxID3'];
                        }
                        if(isset($TID2['LR'])){
                            $data['LR_2'] = $TID2['LR'];
                        }
                        if(isset($TID2['NSU'])){
                            $data['NSU_2'] = $TID2['NSU'];
                        }                         
                    }
                }
            }
        }
        
        return $transport->setData(array_merge($data, $transport->getData()));
    }
    
}