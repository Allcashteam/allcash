<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('allcash_mc/payment'), 'capture_result', 'text');

$installer->getConnection()
    ->addColumn($installer->getTable('allcash_mc/payment'), 'capture_result2', 'text');

$installer->endSetup();