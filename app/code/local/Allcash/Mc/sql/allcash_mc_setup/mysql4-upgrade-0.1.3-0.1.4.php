<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('allcash_mc/payment'), 'increment_id', 'VARCHAR(50) AFTER order_id');

$installer->endSetup();