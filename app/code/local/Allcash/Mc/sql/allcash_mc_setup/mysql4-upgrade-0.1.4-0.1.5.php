<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('allcash_mc/payment'), 'cc_type', 'VARCHAR(20) AFTER type');

$installer->endSetup();