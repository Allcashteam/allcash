<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('allcash_mc/payment'), 'store_id', 'int(11) NOT NULL');

$installer->endSetup();

