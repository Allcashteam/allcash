<?php

$installer = $this;
$installer->startSetup();


$installer->getConnection()
    ->addColumn($installer->getTable('allcash_mc/payment'), 'info2', 'text AFTER info');

$installer->getConnection()
    ->addColumn($installer->getTable('allcash_mc/payment'), 'cc_type2', 'VARCHAR(20) after cc_type');

$installer->getConnection()
    ->addColumn($installer->getTable('allcash_mc/payment'), 'clearsale_info', 'text AFTER registration_cc');

$installer->endSetup();