<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('allcash_mc/payment'), 'registration_cc', 'text');

$installer->endSetup();