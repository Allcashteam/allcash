<?php

class Allcash_Mc_Model_Cron extends Mage_Core_Model_Abstract {
    
    public function getStoreIds(){
        return Mage::getModel('core/store')->getCollection()->load()->getAllIds();
    }

    public function processPaymentCc(){
        foreach($this->getStoreIds() as $storeId){
            Mage::getModel('allcash_mc/mc')->authorize($storeId);
            Mage::getModel('allcash_mc/mc')->captureCc($storeId);
        }
    }       
    
    public function captureBoleto(){   
        foreach($this->getStoreIds() as $storeId){
            Mage::getModel('allcash_mc/mc')->captureBoleto($storeId);
        }
    }    
    
    
}