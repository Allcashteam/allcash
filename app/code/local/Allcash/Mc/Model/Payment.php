<?php

/**
 * Allcash + Conversão Module
 *
 * @title      Magento -> + Conversão Module
 * @category   Payment Gateway
 * @package    Allcash_Mc
 * @author     Allcash Team
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @copyright  Copyright (c) 2013 Allcash
 */
class Allcash_Mc_Model_Payment extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('allcash_mc/payment');
    }

}
