<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('clearsale/orders'), 'store_id', 'int(11) NOT NULL');
$installer->endSetup();
