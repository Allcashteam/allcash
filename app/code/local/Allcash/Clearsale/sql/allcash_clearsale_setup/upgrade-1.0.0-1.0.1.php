<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
->addColumn($installer->getTable('sales_flat_order'), 'prazo_entrega', 'varchar(50) NULL');

$installer->endSetup();
