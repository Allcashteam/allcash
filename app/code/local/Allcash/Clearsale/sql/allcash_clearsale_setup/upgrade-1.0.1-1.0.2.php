<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
->addColumn($installer->getTable('clearsale/orders'), 'score', 'varchar(10) NULL AFTER status_clearsale');
$installer->endSetup();
