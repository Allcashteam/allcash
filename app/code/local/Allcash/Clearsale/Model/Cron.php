<?php

class Allcash_Clearsale_Model_Cron extends Mage_Core_Model_Abstract {
    
    public function getStoreIds(){
        return Mage::getModel('core/store')->getCollection()->load()->getAllIds();
    }
    
    public function getOrdersStatus(){
        
        foreach($this->getStoreIds() as $storeId){
            if(Mage::getStoreConfig('allcash/clearsale/active',$storeId)){
                if(Mage::getStoreConfig('allcash/clearsale/produto',$storeId) == 'tg'){
                    Mage::getModel('clearsale/clearsale')->getOrdersStatus($storeId);
                }
            }
        }
    }    

    public function sendOrders(){
        
        foreach($this->getStoreIds() as $storeId){        
            if(Mage::getStoreConfig('allcash/clearsale/active',$storeId)){
                if(Mage::getStoreConfig('allcash/clearsale/produto',$storeId) == 'tg'){
                    Mage::getModel('clearsale/clearsale')->sendOrders($storeId);
                }
            }
        }
    }    
    
}