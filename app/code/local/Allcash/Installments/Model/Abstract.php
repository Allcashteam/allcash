<?php

class Allcash_Installments_Model_Abstract extends Varien_Object {

    
    private $_value;
    private $_installmentsArray;
     
    public function __construct() {
        parent::__construct();
        $this->setMaxInstallment($this->_getMaxParcelas());
        $this->setTaxaJuros($this->_getTaxaJuros()); 
    }
    
    public function getStoreId(){
        return Mage::app()->getStore()->getStoreId();
    }      
    public function isActive() {
        return Mage::getStoreConfig('allcash/installments/active',$this->getStoreId()) ? true : false;
    }

    /*
     * System->Configuration getters
     */

    public function getMessage($juros) {
        if ($juros > 0) {
            return sprintf(Mage::getStoreConfig('allcash/installments/mensagem_padrao_comjuros',$this->getStoreId()), number_format($juros, 2) . '%' );
        } else {
            return Mage::getStoreConfig('allcash/installments/mensagem_padrao_semjuros',$this->getStoreId());
        }
    }

    public function getParcelaConfigurationArray() {
        if (!$this->_installmentsArray) {
            $this->_installmentsArray = unserialize(Mage::getStoreConfig('allcash/installments/parcelas',$this->getStoreId()));
        }
        return $this->_installmentsArray;
    }
    
    
    public function getParcelaMinima() {
        return Mage::getStoreConfig('allcash/installments/parcela_minima',$this->getStoreId());
    }
    
    public function getParcelaSemJuros() {
        return Mage::getStoreConfig('allcash/installments/n_parcelas_s_juros',$this->getStoreId());
    }
    
    public function _getTaxaJuros() {
        return Mage::getStoreConfig('allcash/installments/taxa_juros',$this->getStoreId());
    }
    
    public function setValue($value) {
        $this->_value = $value;
        return $this;
    }

    public function getValue() {
        if (!$this->_value) {
            if( Mage::getSingleton('checkout/session')->getBaseTotal() ){
                $this->_value = Mage::getSingleton('checkout/session')->getBaseTotal();
            }else{
                $this->_value = Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal();
                Mage::getSingleton('checkout/session')->setBaseTotal( Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal() );
            }
            
        }
        return $this->_value;
    }
    
     public function getInstallmentSequence() {

        //$cacheName = 'allcashInstallments-'.$this->getTaxaJuros().'-'.$this->getValue().'-'.$this->getMaxInstallment().'-'.$this->getParcelaMinima().'-'.$this->getParcelaSemJuros().'-'.$this->getMessage(0).'-'.$this->getMessage(1).Mage::helper('core')->currency($this->getValue());
        //$cache = Mage::getSingleton('core/cache');
        //if ($cachedObject = $cache->load($cacheName)) {
        //    return unserialize($cachedObject);
        //}
        
        try{
         
            $installments = Mage::getModel('installments/installmentset');

            if ($this->getValue() <= 0) {
                return $installments;
            }

            $juros = $this->getTaxaJuros() / 100;
            $capital = $this->getValue();
            $divided = 0;

            $max = $this->getMaxInstallment();  
            $min = $this->getParcelaMinima();   // 5

            $installmentDiscounts = $this->getInstallmentDiscounts();    

            for ($parcela = 1; $parcela <= $max; $parcela++) {

                //If installment discount exists
                if(sizeof($installmentDiscounts)){ 

                    //Check if current installment has discount
                    $hasInstallment = false;
                    foreach($installmentDiscounts as $item){
                        if($item['inst'] == $parcela){
                            $hasInstallment = true;
                        }
                    }      

                    //Use Total Base with shipping added
                    $totals = Mage::getSingleton('checkout/session')->getQuote()->getTotals(); 
                    $subtotal = $totals["subtotal"]->getValue();                
                    $shipping = $totals['shipping']->getValue();
                    $grandTotalWithoutDiscounts = $subtotal+$shipping;

                    if($hasInstallment){
                        $capital = $grandTotalWithoutDiscounts - (($item['value']/100)*$subtotal);
                    }else{
                        $capital = $grandTotalWithoutDiscounts;
                    }
                }

                if ($parcela == 1) {
                    $installments->pushInstallment($capital, $this->getMessage(0));
                } else {

                    //Defines whether the installment will be with or without interest
                    if ($juros > 0 && ($parcela) > $this->getParcelaSemJuros()) {
                        $divided = ($capital) * ( ($juros) / ( 1 - (1 / pow( (1+$juros), $parcela ))));
                        $jurosMessage = $this->getTaxaJuros();
                    } else {
                        $divided = $capital / $parcela;
                        $jurosMessage = '0';

                    }

                    if ($divided >= floatval($min)) {
                        $installments->pushInstallment($divided,  $this->getMessage($jurosMessage), $parcela);
                    }

                    if ($parcela == Mage::getStoreConfig('allcash/installments/n_parcelas_s_juros',$this->getStoreId()) &&
                            Mage::getStoreConfig('allcash/installments/active_installment_type',$this->getStoreId()) == 'semjuros' &&
                                Mage::app()->getRequest()->getModuleName() != 'onestepcheckout' &&
                                    !$this->getHasInstallment()) {
                        break;
                    }

                }

                //Break if was reached min installment allowed
                if(($divided > 0) && ($divided < $min)){
                    break;
                }

            }

        }catch(Exception $e){
            Mage::log(__METHOD__.' | Error: '.$e->getMessage(),null,'allcash_error.log');
        }
        
        //$cache->save(serialize($installments),$cacheName,array('allcashInstallmentsCache'));
        return $installments;
    }

    public function getInstallmentHighest() {
        $installments = $this->getInstallmentSequence();
        $last = false;

        foreach ($installments->returnIterable() as $installment) {
            $last = $installment;
        }

        return $last;
    }

    public function getInstallmentByItem( $parcela ) {
        $installments = $this->getInstallmentSequence();
        $last = false;

        foreach ($installments->returnIterable() as $installment) {
            $last = $installment;
            if( $installment->getValue() == $parcela ){
                break;
            }
        }

        return $last;
    }

    private function getInstallmentDiscounts(){

        $cc_installments = Mage::registry('cc_installments');
        if(!empty($cc_installments)){
            $read = Mage::getSingleton('core/resource')->getConnection('core_read');
            $query = "SELECT * FROM salesrule WHERE name LIKE '%allcash_cc%'";
            $rules = $read->fetchAll($query);
            Mage::register('cc_installments',$rules);
        }else{
            $rules = Mage::registry('cc_installments');
        }
        
        $hasInstallmentsDiscount = false;
        $installmentsDiscount = array();
        if(sizeof($rules)){
            foreach ($rules as $rule) {
                
                $conditions = unserialize($rule['conditions_serialized']);
                
                if(isset($conditions['conditions'])){
                    foreach($conditions['conditions'] as $condition){
                        
                        if(is_array($condition)){
                            foreach($condition['conditions'] as $subcondition){
                                //Mage::log(print_r($subcondition,true),null,'ruleIds.log');
                                $infos = array();
                                if($subcondition['attribute'] == 'installments' && $subcondition['operator'] == '=='){
                                    $infos['inst'] = $subcondition['value'];
                                }
                            }
                            if(isset($infos['inst'])){
                                $infos['value'] = $rule['discount_amount'];
                                $installmentsDiscount[] = $infos;
                                break;
                            }
                        }
                    }
                }
            }                    
        }
        //Mage::getSingleton('checkout/session')->setInstallmentDiscounts($installmentsDiscount);
        return $installmentsDiscount;
    }
    
}