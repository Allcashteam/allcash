<?php

class Allcash_Installments_Model_Auto extends Allcash_Installments_Model_Abstract {

    public function _getMaxParcelas() {
        return Mage::getStoreConfig('allcash/installments/n_max_parcelas',Mage::app()->getStore()->getStoreId());
    }

}
