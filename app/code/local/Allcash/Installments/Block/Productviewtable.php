<?php

class Allcash_Installments_Block_Productviewtable extends Allcash_Installments_Block_Abstract
{
	public function _construct()
	{
		$this->setTemplate('allcash_installments/productviewtable.phtml');
	}
	
	public function getInstallments()
	{
            	$this->getModel()->setValue($this->getValue());
                return $this->_getInstallments()->returnIterable();
	}
}