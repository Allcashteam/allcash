<?php

class Allcash_Installments_Block_Formcustom extends Allcash_Installments_Block_Abstract {

    public function _construct() {
        $this->setTemplate('allcash_installments/formcustom.phtml');
    }

    public function isOnestep() {
        return Mage::getStoreConfig('onestepcheckout/general/rewrite_checkout_links');
    }

    public function isInovartCheckout5() {
        return Mage::getStoreConfig('onepagecheckout/general/enabled');
    }

    public function isInovartCheckout6() {
        return Mage::getStoreConfig('onestepcheckout/general/is_enabled');
    }

    public function getInstallments($getFromSession = false) {
        return $this->_getInstallments()->returnIterable();
    }

    public function getFormType() {
        return Mage::getStoreConfig('allcash/installments/form_type',Mage::app()->getStore()->getStoreId());
    }

}
