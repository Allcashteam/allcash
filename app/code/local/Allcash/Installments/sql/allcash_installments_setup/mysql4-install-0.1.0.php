<?php

/**
 * Allcash + Conversão Module
 *
 * @title      Magento -> + Conversão Module
 * @category   Payment Gateway
 * @package    Allcash_Mc
 * @author     Allcash Team
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @copyright  Copyright (c) 2013 Allcash
 */
$installer = $this;
$installer->startSetup();
    $installer->getConnection()
        ->addColumn($installer->getTable('sales_flat_quote_address'), 'juros_amount', 'DECIMAL(12,4)');
    $installer->getConnection()
        ->addColumn($installer->getTable('sales_flat_quote_address'), 'base_juros_amount', 'DECIMAL(12,4)');
    $installer->getConnection()
        ->addColumn($installer->getTable('sales_flat_order'), 'juros_amount', 'DECIMAL(12,4)');
    $installer->getConnection()
        ->addColumn($installer->getTable('sales_flat_order'), 'base_juros_amount', 'DECIMAL(12,4)');
    $installer->getConnection()
        ->addColumn($installer->getTable('sales_flat_invoice'), 'juros_amount', 'DECIMAL(12,4)');
    $installer->getConnection()
        ->addColumn($installer->getTable('sales_flat_invoice'), 'base_juros_amount', 'DECIMAL(12,4)');
    $installer->getConnection()
        ->addColumn($installer->getTable('sales_flat_creditmemo'), 'juros_amount', 'DECIMAL(12,4)');
    $installer->getConnection()
        ->addColumn($installer->getTable('sales_flat_creditmemo'), 'base_juros_amount', 'DECIMAL(12,4)');
    $installer->getConnection()
        ->addColumn($installer->getTable('sales_flat_order_payment'), 'cc_parcelas', 'INT(11) DEFAULT NULL');
    $installer->getConnection()
        ->addColumn($installer->getTable('sales_flat_quote_payment'), 'cc_parcelas', 'INT(11) DEFAULT NULL');
    $installer->getConnection()
        ->addColumn($installer->getTable('sales_flat_order_payment'), 'gwap_boleto_type', 'VARCHAR(255) DEFAULT NULL');
    $installer->getConnection()
        ->addColumn($installer->getTable('sales_flat_quote_payment'), 'gwap_boleto_type', 'VARCHAR(255) DEFAULT NULL');

    $installer->endSetup();
