<?php

class Allcash_Installments_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getInstallmentModel()
    {
       
        $mode = Mage::getStoreConfig('allcash/installments/active',Mage::app()->getStore()->getStoreId());

        $instance = Mage::getModel('installments/'.$mode);
        if( ! ($instance instanceof  Allcash_Installments_Model_Abstract ) ){
            Mage::throwException( printf( Mage::helper('installments')->__(  'Erro ao instanciar model: %s'), $mode ) ) ;
        }

        return $instance;
    }
}
